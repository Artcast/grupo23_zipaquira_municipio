<?php
/**
 * @copyright Copyright (C) 2009-2013 inch communications ltd. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
if(version_compare(JVERSION, '3.0', 'ge')) {
	JHtml::_('bootstrap.tooltip');
	JHtml::_('dropdown.init');
	JHtml::_('formbehavior.chosen', 'select');
	JHtml::_('behavior.multiselect');

} else {
	JHtml::_('behavior.tooltip');
}
	JHtml::_('behavior.formvalidation');
$canDo	= FlexbannersHelper::getActions();
if(version_compare(JVERSION, '3.0', 'ge')) {
$script = "
	jQuery(document).ready(function ($){
		$('#jform_type').change(function(){
		if(document.adminForm.jform_type.value == '0') {
				document.adminForm.imagelib.src = '../images/banners/' + document.adminForm.jform_imageurl.value;
			} else if(document.adminForm.jform_type.value == '2') {
				document.adminForm.imagelib.src = document.adminForm.jform_cloud_imageurl.value;
			} else {
				document.adminForm.imagelib.src = '../images/blank.png';
			}
		}).trigger('change');
	});";
} else { 
$script = "
		window.addEvent('domready', function() {
		document.id('jform_type').addEvent('change', function() {
			if(document.adminForm.jform_type.value == '0') {
				document.adminForm.imagelib.src = '../images/banners/' + document.adminForm.jform_imageurl.value;
			} else if(document.adminForm.jform_type.value == '2') {
				document.adminForm.imagelib.src = document.adminForm.jform_cloud_imageurl.value;
			} else {
				document.adminForm.imagelib.src = '../images/blank.png';
			}
		});
	});";
 } 
// Add the script to the document head.
JFactory::getDocument()->addScriptDeclaration($script);

?>

<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		if(task == 'flexbanner.cancel' || document.formvalidator.isValid(document.id('banner-form'))) {
			Joomla.submitform(task, document.getElementById('banner-form'));
		}
	}
	window.addEvent('domready', function() {
		document.id('jform_type0').addEvent('click', function(e) {
			document.id('image').setStyle('display', 'block');
			document.id('cloud_image').setStyle('display', 'none');
			document.id('linkurl').setStyle('display', 'block');
			document.id('custom').setStyle('display', 'none');
		});
		document.id('jform_type1').addEvent('click', function(e) {
			document.id('image').setStyle('display', 'none');
			document.id('cloud_image').setStyle('display', 'none');
			document.id('linkurl').setStyle('display', 'none');
			document.id('custom').setStyle('display', 'block');
		});
		document.id('jform_type2').addEvent('click', function(e) {
			document.id('image').setStyle('display', 'none');
			document.id('cloud_image').setStyle('display', 'block');
			document.id('linkurl').setStyle('display', 'block');
			document.id('custom').setStyle('display', 'none');
		});
		if(document.id('jform_type0').checked == true) {
			document.id('jform_type0').fireEvent('click');
		} else if(document.id('jform_type2').checked == true) {
			document.id('jform_type2').fireEvent('click');
		} else {
			document.id('jform_type1').fireEvent('click');
		}
	});

</script>

<form action="<?php echo JRoute::_('index.php?option=com_flexbanners&layout=edit&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="banner-form" class="form-validate form-horizontal">
<?php if(version_compare(JVERSION, '3.0', 'ge')) { ?>
	<!-- Begin Banner -->
	<div class="span10 form-horizontal">
		<fieldset>
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#details" data-toggle="tab"><?php echo empty($this->item->id) ? JText::_('COM_FLEXBANNERS_MANAGER_BANNER_NEW') : JText::sprintf('COM_FLEXBANNERS_MANAGER_BANNER_EDIT', $this->item->id);?></a>
				</li>
				<li>
					<a href="#restrictions" data-toggle="tab"><?php echo JText::_('ADMIN_FLEXBANNER_CONTENTRESTRICTIONS');?></a>
				</li>
				<li><a href="#otherparams" data-toggle="tab"><?php echo JText::_('ADMIN_FLEXBANNER_BANNER_DETAILS');?></a></li>
				<li>
					<a href="#publishing" data-toggle="tab"><?php echo JText::_('COM_FLEXBANNERS_GROUP_LABEL_PUBLISHING_DETAILS');?></a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="details">
					<?php echo $this -> form -> getControlGroup('name');?>
					<?php echo $this -> form -> getControlGroup('catid');?>
					<?php foreach($this->form->getFieldset('flexbannerclient') as $field): ?>
					<div class="control-group">
						<div class="control-label">
							<?php echo $field -> label;?>
						</div>
						<div class="controls">
							<?php echo $field -> input;?>
						</div>
					</div>
					<?php endforeach;?>
					<?php echo $this -> form -> getControlGroup('locationid');?>
					<?php echo $this -> form -> getControlGroup('sizeid');?>
					<div class="control-group">
						<div class="control-label">
							<?php echo $this -> form -> getLabel('type');?>
						</div>
						<div class="controls">
							<?php echo $this -> form -> getInput('type');?>
						</div>

						<div id="image">
							<div class="row-fluid">
								<div class="control-label">
									<?php echo $this -> form -> getLabel('imageurl');?>
								</div>
								<div class="controls">
									<?php echo $this -> form -> getInput('imageurl');?>
								</div>
							</div>
							<div class="row-fluid">
								<div class="control-label">
									<label><?php echo JText::_('ADMIN_FLEXBANNER_BANNERIMAGE');?></label>
								</div>
								<div class="controls">
									<?php if (preg_match("/swf|html/", $this->item->imageurl)) {
									?><img src="../images/blank.png" name="imagelib">
									<?php
									} elseif (preg_match("/gif|jpg|png/", $this->item->imageurl)) {
									?><img src="../images/banners/<?php echo $this->item->imageurl;?>" name="imagelib" />
									<?php
									} else {
									?><img src="../images/blank.png" name="imagelib" />
									<?php
									}
									?>
								</div>
							</div>
						</div>
						<div id="cloud_image">
							<div class="row-fluid">
								<div class="control-label">
									<?php echo $this -> form -> getLabel('cloud_imageurl');?>
								</div>
								<div class="controls">
									<?php echo $this -> form -> getInput('cloud_imageurl');?>
								</div>
							</div>
							<div class="row-fluid">
								<div class="control-label">
									<label><?php echo JText::_('ADMIN_FLEXBANNER_BANNERIMAGE');?></label>
								</div>
								<div class="controls">
									<?php if (preg_match("/swf|html/", $this->item->cloud_imageurl)) {
									?><img src="../images/blank.png" name="imagelib">
									<?php
									} elseif (preg_match("/gif|jpg|png/", $this->item->cloud_imageurl)) {
									?><img src="<?php echo $this->item->cloud_imageurl;?>" name="imagelib" />
									<?php
									} else {
									?><img src="../images/blank.png" name="imagelib" />
									<?php
									}
									?>
								</div>
							</div>
						</div>
					</div>
					<div id="linkurl">
						<div class="row-fluid">
							<div class="control-label">
								<?php echo $this -> form -> getLabel('linkid');?>
							</div>
							<div class="controls">
								<?php echo $this -> form -> getInput('linkid');?>
							</div>
						</div>
						<div class="row-fluid">
							<div class="control-label">
								<?php echo $this -> form -> getControlGroup('imagealt');?>
							</div>
						</div>
						<div class="row-fluid">
							<div class="control-label">
								<?php echo $this -> form -> getLabel('newwin');?>
							</div>
							<div class="controls">
								<?php echo $this -> form -> getInput('newwin');?>
							</div>
						</div>
					</div>
					<div id="custom">
						<div class="control-group">
							<div class="control-label">
								<?php echo $this -> form -> getLabel('customcode');?>
							</div>
							<div class="controls">
								<?php echo $this -> form -> getInput('customcode');?>
							</div>
						</div>
					</div>
					<div class="control-group">
						<div class="control-label">
							<?php echo $this -> form -> getLabel('id');?>
						</div>
						<div class="controls">
							<?php echo $this -> form -> getInput('id');?>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="restrictions">
					<?php foreach($this->form->getFieldset('flexbanner') as $field):
					?>
					<div class="control-group">
						<div class="control-label">
							<?php echo $field -> label;?>
						</div>
						<div class="controls">
							<?php echo $field -> input;?>
						</div>
					</div>
					<?php endforeach;?>
				</div>
				<div class="tab-pane" id="otherparams">
					<?php foreach ($this->form->getFieldset('otherparams') as $field) :
					?>
					<div class="control-group">
						<div class="control-label">
							<?php echo $field -> label;?>
						</div>
						<div class="controls">
							<?php echo $field -> input;?>
						</div>
					</div>
					<?php endforeach;?>
				</div>
				<div class="tab-pane" id="publishing">
					<?php foreach ($this->form->getFieldset('publish') as $field) :
					?>
					<div class="control-group">
						<div class="control-label">
							<?php echo $field -> label;?>
						</div>
						<div class="controls">
							<?php echo $field -> input;?>
						</div>
					</div>
					<?php endforeach;?>
				</div>
		</fieldset>
		<input type="hidden" name="task" value="" />
		<?php echo JHtml::_('form.token');?>
	</div>
	<!-- End Newsfeed -->
	<!-- Begin Sidebar -->
	<div class="span2">
		<h4><?php echo JText::_('JDETAILS');?></h4>
		<hr />
		<fieldset class="form-vertical">
			<div class="control-group">
				<div class="controls">
					<?php echo $this->form->getValue('name');?>
				</div>
			</div>
			<div class="control-group">
				<div class="control-label">
					<?php echo $this->form->getLabel('state');?>
				</div>
				<div class="controls">
					<?php echo $this->form->getInput('state');?>
				</div>
			</div>
			<div class="control-group">
				<div class="control-label">
					<?php echo $this->form->getLabel('language');?>
				</div>
				<div class="controls">
					<?php echo $this->form->getInput('language');?>
				</div>
			</div>
		</fieldset>
	</div>
	<!-- End Sidebar -->
<?php } else { ?>
		<div class="width-60 fltlft">
		<fieldset class="adminform">
			<legend>
				<?php echo  JText::_('COM_FLEXBANNERS_BANNER_DETAILS')
				?>
			</legend>
			<ul class="adminformlist">
				<li>
					<?php echo $this -> form -> getLabel('name');?>
					<?php echo $this -> form -> getInput('name');?>
				</li>
				<li>
					<?php echo $this -> form -> getLabel('access');?>
					<?php echo $this -> form -> getInput('access');?>
				</li>
				<li>
					<?php echo $this -> form -> getLabel('catid');?>
					<?php echo $this -> form -> getInput('catid');?>
				</li>
				<?php foreach($this->form->getFieldset('flexbannerclient') as $field):
				?>
				<li>
					<?php echo $field -> label;?>
					<?php echo $field -> input;?>
				</li>
				<?php endforeach;?>

				<li>
					<?php echo $this -> form -> getLabel('locationid');?>
					<?php echo $this -> form -> getInput('locationid');?>
				</li>
				<li>
					<?php echo $this -> form -> getLabel('sizeid');?>
					<?php echo $this -> form -> getInput('sizeid');?>
				</li>
				<li>
					<?php echo $this -> form -> getLabel('state');?>
					<?php echo $this -> form -> getInput('state');?>
				</li>
				<li>
					<?php echo $this -> form -> getLabel('type');?>
					<?php echo $this -> form -> getInput('type');?>
				</li>
				<li>
					<div id="image">
						<div class="control-label">
							<?php echo $this -> form -> getLabel('imageurl');?>
						</div>
						<div class="controls">
							<?php echo $this -> form -> getInput('imageurl');?>
						</div>
						<div class="control-label">
							<label><?php echo JText::_('ADMIN_FLEXBANNER_BANNERIMAGE');?></label>
						</div>
						<div class="controls">
							<?php if (preg_match("/swf|html/", $this->item->imageurl)) {
							?><img src="../images/blank.png" name="imagelib">
							<?php
							} elseif (preg_match("/gif|jpg|png/", $this->item->imageurl)) {
							?><img src="../images/banners/<?php echo $this->item->imageurl;?>" name="imagelib" />
							<?php
							} else {
							?><img src="../images/blank.png" name="imagelib" />
							<?php
							}
							?>
						</div>
						<div id="linkid">
							<?php echo $this -> form -> getLabel('linkid');?>
							<?php echo $this -> form -> getInput('linkid');?>
						</div>
						<div id="imagealt">
							<?php echo $this -> form -> getLabel('imagealt');?>
							<?php echo $this -> form -> getInput('imagealt');?>
						</div>
						<div id="newwin">
							<?php echo $this -> form -> getLabel('newwin');?>
							<?php echo $this -> form -> getInput('newwin');?>
						</div>
					</div>
					<div id="cloud_image">
						<div class="control-label">
							<?php echo $this -> form -> getLabel('cloud_imageurl');?>
						</div>
						<div class="controls">
							<?php echo $this -> form -> getInput('cloud_imageurl');?>
						</div>
						<div class="control-label">
							<label><?php echo JText::_('ADMIN_FLEXBANNER_BANNERIMAGE');?></label>
						</div>
						<div class="controls">
							<?php if (preg_match("/swf|html/", $this->item->cloud_imageurl)) {
							?><img src="../images/blank.png" name="imagelib">
							<?php
							} elseif (preg_match("/gif|jpg|png/", $this->item->cloud_imageurl)) {
							?><img src="<?php echo $this->item->cloud_imageurl;?>" name="imagelib" />
							<?php
							} else {
							?><img src="../images/blank.png" name="imagelib" />
							<?php
							}
							?>
						</div>
						<div id="linkid">
							<?php echo $this -> form -> getLabel('linkid');?>
							<?php echo $this -> form -> getInput('linkid');?>
						</div>
						<div id="imagealt">
							<?php echo $this -> form -> getLabel('imagealt');?>
							<?php echo $this -> form -> getInput('imagealt');?>
						</div>
						<div id="newwin">
							<?php echo $this -> form -> getLabel('newwin');?>
							<?php echo $this -> form -> getInput('newwin');?>
						</div>
					</div>
				</li>
				<li>
					<div id="custom">
						<?php echo $this -> form -> getLabel('customcode');?>
						<?php echo $this -> form -> getInput('customcode');?>
					</div>
				</li>
				<li>
					<?php echo $this -> form -> getLabel('language');?>
					<?php echo $this -> form -> getInput('language');?>
				</li>
				<li>
					<?php echo $this -> form -> getLabel('id');?>
					<?php echo $this -> form -> getInput('id');?>
				</li>
			</ul>
			<div class="clr"></div>
		</fieldset>
		<fieldset class="adminform">
			<legend>
				<?php echo JText::_('ADMIN_FLEXBANNER_CONTENTRESTRICTIONS');?>
			</legend>
			<ul class="adminformlist">
				<?php foreach($this->form->getFieldset('flexbanner') as $field):
				?>
				<li>
					<?php echo $field -> label;?>
					<?php echo $field -> input;?>
				</li>
				<?php endforeach;?>
			</ul>
			<div class="clr"></div>
		</fieldset>
	</div>
	<div class="width-40 fltrt">
		<?php echo JHtml::_('sliders.start', 'banner-sliders-' . $this -> item -> id, array('useCookie' => 1));?>

		<?php echo JHtml::_('sliders.panel', JText::_('COM_FLEXBANNERS_GROUP_LABEL_PUBLISHING_DETAILS'), 'publishing-details');?>
		<fieldset class="panelform">
			<ul class="adminformlist">
				<?php foreach($this->form->getFieldset('otherparams') as $field):
				?>
				<li>
					<?php echo $field -> label;?>
					<?php echo $field -> input;?>
				</li>
				<?php endforeach;?>
			</ul>
			<ul class="adminformlist">
				<?php foreach($this->form->getFieldset('publish') as $field):
				?>
				<li>
					<?php echo $field -> label;?>
					<?php echo $field -> input;?>
				</li>
				<?php endforeach;?>
			</ul>
		</fieldset>
		<?php echo JHtml::_('sliders.end');?>
		<input type="hidden" name="task" value="" />
		<?php echo JHtml::_('form.token');?>
	</div>
	<div class="clr"></div>
<?php } ?>
</form>
<script type="text/javascript">
	window.onload = function() {
		if(document.adminForm.jform_type.value == 0) {
			document.adminForm.imagelib.src = '../images/banners/' + document.adminForm.jform_imageurl.value;
		} else if(document.adminForm.jform_type.value == 2) {
			document.adminForm.imagelib.src = document.adminForm.jform_cloud_imageurl.value;
		} else {
			document.adminForm.imagelib.src = '../images/blank.png';
		}
	};


	window.addEvent('domready', function() {
		document.id('jform_type').addEvent('change', function() {
			if(document.adminForm.jform_type.value == 0) {
				document.adminForm.imagelib.src = '../images/banners/' + document.adminForm.jform_imageurl.value;
			} else if(document.adminForm.jform_type.value == 2) {
				document.adminForm.imagelib.src = document.adminForm.jform_cloud_imageurl.value;
			} else {
				document.adminForm.imagelib.src = '../images/blank.png';
			}
		});
	});

</script>